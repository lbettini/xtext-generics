package org.xtext.example.generics.tests

import com.google.inject.Inject
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.TemporaryFolder
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.xbase.compiler.CompilationTestHelper
import org.junit.Rule
import org.junit.runner.RunWith
import org.xtext.example.generics.GenericsInjectorProvider
import org.junit.Test
import org.eclipse.xtext.xbase.compiler.CompilationTestHelper.Result
import org.eclipse.xtext.util.IAcceptor
import org.eclipse.xtext.diagnostics.Severity
import com.google.common.base.Joiner

import static extension org.junit.Assert.*

@RunWith(XtextRunner)
@InjectWith(GenericsInjectorProvider)
class GenericsCompilerTest {
	
	@Rule
	@Inject public TemporaryFolder temporaryFolder 
	@Inject extension CompilationTestHelper compilationTestHelper

	@Test
	def void testSimpleEntity() {
		'''
		entity MyEntity {
			name : String
		}
		'''.
		assertCorrectJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity {
  public String getName();
  
  public void setName(final String name);
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl implements MyEntity {
  private String name;
  
  public String getName() {
    return this.name;
  }
  
  public void setName(final String name) {
    this.name = name;
  }
}
'''
		)
	}

	@Test
	def void testSimpleEntityWithBooleanProperty() {
		'''
		entity MyEntity {
			name : boolean
		}
		'''.
		assertCorrectJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity {
  public boolean isName();
  
  public void setName(final boolean name);
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl implements MyEntity {
  private boolean name;
  
  public boolean isName() {
    return this.name;
  }
  
  public void setName(final boolean name) {
    this.name = name;
  }
}
'''
		)
	}

	@Test
	def void testGenericEntity() {
		'''
		entity MyEntity<T> {
			name : String
		}
		'''.
		assertCorrectJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity<T extends Object> {
  public String getName();
  
  public void setName(final String name);
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl<T extends Object> implements MyEntity<T> {
  private String name;
  
  public String getName() {
    return this.name;
  }
  
  public void setName(final String name) {
    this.name = name;
  }
}
'''
		)
	}

	@Test
	def void testGenericEntityWithUpperBound() {
		'''
		entity MyEntity<T extends Comparable<T>> {
			
		}
		'''.
		assertGeneratedJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity<T extends Comparable<T>> {
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl<T extends Comparable<T>> implements MyEntity<T> {
}
'''
		)
	}

	@Test
	def void testGenericEntityWithUpperBounds() {
		'''
		entity MyEntity<T extends Comparable<T>, U extends Comparable<U>> {
			
		}
		'''.
		assertGeneratedJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity<T extends Comparable<T>, U extends Comparable<U>> {
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl<T extends Comparable<T>, U extends Comparable<U>> implements MyEntity<T, U> {
}
'''
		)
	}

	@Test
	def void testGenericEntityWithMutualUpperBounds() {
		'''
		entity MyEntity<T extends Comparable<U>, U extends Comparable<T>> {
			
		}
		'''.
		assertGeneratedJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity<T extends Comparable<U>, U extends Comparable<T>> {
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl<T extends Comparable<U>, U extends Comparable<T>> implements MyEntity<T, U> {
}
'''
		)
	}

	@Test
	def void testGenericEntityWithMutualWildcards() {
		'''
		entity MyEntity<T extends Comparable<? extends U>, U extends Comparable<? super T>> {
			
		}
		'''.
		assertGeneratedJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity<T extends Comparable<? extends U>, U extends Comparable<? super T>> {
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl<T extends Comparable<? extends U>, U extends Comparable<? super T>> implements MyEntity<T, U> {
}
'''
		)
	}

	@Test
	def void testGenericProperty() {
		'''
		entity MyEntity<T> {
			name : T
		}
		'''.
		assertGeneratedJavaCode(
'''
@SuppressWarnings("all")
public interface MyEntity<T extends Object> {
  public T getName();
  
  public void setName(final T name);
}
''',
'''
@SuppressWarnings("all")
public class MyEntityImpl<T extends Object> implements MyEntity<T> {
  private T name;
  
  public T getName() {
    return this.name;
  }
  
  public void setName(final T name) {
    this.name = name;
  }
}
'''
		)
	}

	@Test
	def void testGenericEntityWithOperation() {
		'''
		entity MyEntity<T> {
			op m(java.util.List<T> l) : T {
				return l.get(0)
			}
		}
		'''.
		assertGeneratedJavaCode(
'''
import java.util.List;

@SuppressWarnings("all")
public interface MyEntity<T extends Object> {
  public abstract T m(final List<T> l);
}
''',
'''
import java.util.List;

@SuppressWarnings("all")
public class MyEntityImpl<T extends Object> implements MyEntity<T> {
  public T m(final List<T> l) {
    return l.get(0);
  }
}
'''
		)
	}

	@Test
	def void testGenericOperation() {
		'''
		entity MyEntity {
			<T> op m(java.util.List<T> l) : T {
				return l.get(0)
			}
		}
		'''.
		assertGeneratedJavaCode(
'''
import java.util.List;

@SuppressWarnings("all")
public interface MyEntity {
  public abstract <T extends Object> T m(final List<T> l);
}
''',
'''
import java.util.List;

@SuppressWarnings("all")
public class MyEntityImpl implements MyEntity {
  public <T extends Object> T m(final List<T> l) {
    return l.get(0);
  }
}
'''
		)
	}

	@Test
	def void testGenericOperationShadowingEntityTypeParameter() {
		'''
		entity MyEntity<T> {
			<T extends String> op m(java.util.List<T> l) : String {
				return l.get(0)
			}
		}
		'''.
		assertGeneratedJavaCode(
'''
import java.util.List;

@SuppressWarnings("all")
public interface MyEntity<T extends Object> {
  public abstract <T extends String> String m(final List<T> l);
}
''',
'''
import java.util.List;

@SuppressWarnings("all")
public class MyEntityImpl<T extends Object> implements MyEntity<T> {
  public <T extends String> String m(final List<T> l) {
    return l.get(0);
  }
}
'''
		)
	}

	@Test
	def void testGenericFunctionTypeRefs() {
		'''
		entity MyEntity<T, U> {
			f : (T) => U
			
			op m(T p) : U {
				return f.apply(p)
			}
		}
		'''.
		assertGeneratedJavaCode(
'''
import org.eclipse.xtext.xbase.lib.Functions.Function1;

@SuppressWarnings("all")
public interface MyEntity<T extends Object, U extends Object> {
  public Function1<? super T, ? extends U> getF();
  
  public void setF(final Function1<? super T, ? extends U> f);
  
  public abstract U m(final T p);
}
''',
'''
import org.eclipse.xtext.xbase.lib.Functions.Function1;

@SuppressWarnings("all")
public class MyEntityImpl<T extends Object, U extends Object> implements MyEntity<T, U> {
  private Function1<? super T, ? extends U> f;
  
  public Function1<? super T, ? extends U> getF() {
    return this.f;
  }
  
  public void setF(final Function1<? super T, ? extends U> f) {
    this.f = f;
  }
  
  public U m(final T p) {
    return this.f.apply(p);
  }
}
'''
		)
	}

	@Test
	def void testTypeParameterReferenceInExpression() {
		'''
		entity MyEntity<T> {
			op m(java.util.List<T> l) : T {
				val T first = l.get(0)
				return first
			}
			
			<U> op m2(java.util.List<U> l) : U {
				val U first = l.get(0)
				return first
			}
		}
		'''.
		assertGeneratedJavaCode(
'''
import java.util.List;

@SuppressWarnings("all")
public interface MyEntity<T extends Object> {
  public abstract T m(final List<T> l);
  
  public abstract <U extends Object> U m2(final List<U> l);
}
''',
'''
import java.util.List;

@SuppressWarnings("all")
public class MyEntityImpl<T extends Object> implements MyEntity<T> {
  public T m(final List<T> l) {
    final T first = l.get(0);
    return first;
  }
  
  public <U extends Object> U m2(final List<U> l) {
    final U first = l.get(0);
    return first;
  }
}
'''
		)
	}

	/**
	 * This will also check that the generated Java code compiles fine
	 */
	def private assertCorrectJavaCode(CharSequence input, CharSequence expectedInterface, CharSequence expectedClass) {
		input.compile(true)[
			expectedInterface.toString.assertEquals(getGeneratedCode("MyEntity"))
			expectedClass.toString.assertEquals(getGeneratedCode("MyEntityImpl"))
			// to compile generated Java code:
			compiledClass
		]
	}

	def private assertGeneratedJavaCode(CharSequence input, CharSequence expectedInterface, CharSequence expectedClass) {
		input.compile(true)[
			expectedInterface.toString.assertEquals(getGeneratedCode("MyEntity"))
			expectedClass.toString.assertEquals(getGeneratedCode("MyEntityImpl"))
		]
	}
	
	def protected compile(CharSequence input, boolean checkValidationErrors, IAcceptor<Result> acceptor) {
		compilationTestHelper.compile(input)[
			if (checkValidationErrors) {
				assertNoValidationErrors
			}
			acceptor.accept(it)
		]
	}
	
	private def assertNoValidationErrors(Result it) {
		val allErrors = getErrorsAndWarnings.filter[severity == Severity.ERROR]
		if (!allErrors.empty) {
			throw new IllegalStateException("One or more resources contained errors : "+
				Joiner.on(',').join(allErrors)
			);
		}
	}
}