package org.xtext.example.generics.tests

import com.google.inject.Inject
import org.eclipse.xtext.common.types.TypesPackage
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.eclipse.xtext.xbase.validation.IssueCodes
import org.junit.Test
import org.junit.runner.RunWith
import org.xtext.example.generics.GenericsInjectorProvider
import org.xtext.example.generics.generics.Program
import org.eclipse.xtext.xbase.XbasePackage

@RunWith(XtextRunner)
@InjectWith(GenericsInjectorProvider)
class GenericsValidatorTest {
	
	@Inject extension ParseHelper<Program>
	@Inject extension ValidationTestHelper
	
	@Test def void testTypeParamReferenceToAnotherEntity() {
		'''
		entity First<T> {
			
		}
		
		entity Second {
			f : T
		}
		'''.toString => [
			val offset = lastIndexOf("T")
			assertErrorStaticReference(offset)
		]
	}

	@Test def void testTypeParamReferenceToAnotherOperation() {
		'''
		entity First {
			<T> op m(T t) : T {
				return null
			}
			op m2() : T {
				return null
			}
		}
		'''.toString => [
			val offset = lastIndexOf("T")
			assertErrorStaticReference(offset)
		]
	}
	
	@Test def void testCorrectTypeParamReference() {
		'''
		entity First<T> {
			f : T
			
			op m(T t) : T {
				return null
			}
		}
		'''.parse.assertNoErrors
	}

	@Test def void testCorrectOpTypeParamReference() {
		'''
		entity First {
			<T> op m(T t) : T {
				return null
			}
		}
		'''.parse.assertNoErrors
	}

	@Test
	def void testGenericEntityWithOperationWithInferredType() {
		// inferred return type won't work with generics.
		'''
		entity MyEntity<T> {
			op m(java.util.List<T> l) {
				return l.get(0)
			}
		}
		'''.
		parse.assertError(
			XbasePackage.Literals.XMEMBER_FEATURE_CALL,
			IssueCodes.INCOMPATIBLE_TYPES,
			"type T is not applicable at this location"
		)
	}


	private def assertErrorStaticReference(String it, int offset) {
		parse.assertError(
			TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE,
			IssueCodes.STATIC_ACCESS_TO_INSTANCE_MEMBER,
			offset, 1,
			"Cannot make a static reference to the non-static type T"
		)
	}
}
