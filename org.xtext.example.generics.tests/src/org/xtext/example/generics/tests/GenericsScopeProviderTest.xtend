package org.xtext.example.generics.tests

import com.google.inject.Inject
import org.eclipse.xtext.common.types.JvmConstraintOwner
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference
import org.eclipse.xtext.common.types.JvmType
import org.eclipse.xtext.common.types.JvmTypeParameter
import org.eclipse.xtext.common.types.JvmTypeParameterDeclarator
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.JvmWildcardTypeReference
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations
import org.eclipse.xtext.xtype.XFunctionTypeRef
import org.junit.Test
import org.junit.runner.RunWith
import org.xtext.example.generics.GenericsInjectorProvider
import org.xtext.example.generics.generics.Program

import static org.junit.Assert.*
import org.eclipse.xtext.junit4.util.ParseHelper
import java.util.List

@RunWith(XtextRunner)
@InjectWith(GenericsInjectorProvider)
class GenericsScopeProviderTest {
	
	@Inject extension IJvmModelAssociations
	@Inject extension ParseHelper<Program>
	
	/** each entity has 1 associated interface and 1 associated class */
	val static INTERFACE_AND_CLASS = 2

	/** each operation has 1 associated Java method in the interface and in the class */
	val static SINGLE_OPERATION = 2

	@Test def void testGenericEntity() {
		'''
		entity MyEntity<T> {
			
		}
		'''.parse.assertJvmGenericTypeTypeParameterBindings(INTERFACE_AND_CLASS)
	}

	@Test def void testGenericEntityWithBound() {
		'''
		entity MyEntity<T extends Comparable<T>> {
			
		}
		'''.parse.assertJvmGenericTypeTypeParameterBindings(INTERFACE_AND_CLASS)
	}

	@Test def void testGenericEntityWithBounds() {
		'''
		entity MyEntity<T extends Comparable<T>, U extends Comparable<U>> {
			
		}
		'''.parse.assertJvmGenericTypeTypeParameterBindings(INTERFACE_AND_CLASS)
	}

	@Test def void testGenericEntityWithMutualBounds() {
		'''
		entity MyEntity<T extends Comparable<U>, U extends Comparable<T>> {
			
		}
		'''.parse.assertJvmGenericTypeTypeParameterBindings(INTERFACE_AND_CLASS)
	}

	@Test def void testGenericEntityWithMutualWildcards() {
		'''
		entity MyEntity<T extends Comparable<? extends U>, U extends Comparable<? super T>> {
			
		}
		'''.parse.assertJvmGenericTypeTypeParameterBindings(INTERFACE_AND_CLASS)
	}

	@Test def void testGenericFunctionTypeRefs() {
		'''
		entity MyEntity<T, U> {
			op m((T) => U p) : (T) => U {
				return null
			}
		}
		'''.parse.assertJvmOperationTypeParameterBoundToContainingType(SINGLE_OPERATION)
	}

	@Test def void testGenericEntityWithOperation() {
		'''
		entity MyEntity<T> {
			op m(java.util.List<T> l) : T {
				return l.get(0)
			}
		}
		'''.parse.assertJvmOperationTypeParameterBoundToContainingType(SINGLE_OPERATION)
	}

	@Test def void testGenericOperation() {
		'''
		entity MyEntity {
			<T> op m(java.util.List<T> l) : T {
				return l.get(0)
			}
		}
		'''.parse.assertJvmOperationTypeParameterBoundToContainingMethod(SINGLE_OPERATION)
	}

	@Test def void testGenericOperationShadowingEntityTypeParameter() {
		'''
		entity MyEntity<T> {
			<T> op m(java.util.List<T> l) : T {
				return l.get(0)
			}
		}
		'''.parse.assertJvmOperationTypeParameterBoundToContainingMethod(SINGLE_OPERATION)
	}

	private def assertJvmGenericTypeTypeParameterBindings(Program it,
		int expectedAssociatedElements
	) {
		val associatedElements = associatedJvmGenericTypes(it)
		// println(associatedElements)
		assertFalse("No associated elements", associatedElements.empty)
		assertEquals(expectedAssociatedElements, associatedElements.size)
		for (t : associatedElements) {
			t.assertJvmGenericTypeTypeParameterBindings[it]
		}
	}

	/**
	 * Retrieves all the JvmGenericTypes associated to the entity
	 */
	private def Iterable<JvmGenericType> associatedJvmGenericTypes(Program it) {
		val programElement = entities.last
		programElement.jvmElements.filter(JvmGenericType)
	}

	private def assertJvmOperationTypeParameterBoundToContainingType(Program it,
			int expectedAssociatedElements) {
		it.assertJvmOperationTypeParameterBindings(expectedAssociatedElements)
			[(it as JvmOperation).declaringType as JvmGenericType]
	}

	private def assertJvmOperationTypeParameterBoundToContainingMethod(Program it,
			int expectedAssociatedElements) {
		it.assertJvmOperationTypeParameterBindings(expectedAssociatedElements)[it]
	}

	private def assertJvmOperationTypeParameterBindings(Program it,
		int expectedAssociatedElements,
		(JvmTypeParameterDeclarator)=>JvmTypeParameterDeclarator expectedTypeParameterDeclarator
	) {
		val associatedElements = associatedJvmOperations(it)
		// println(associatedElements)
		assertFalse("No associated elements", associatedElements.empty)
		assertEquals(expectedAssociatedElements, associatedElements.size)
		for (op : associatedElements) {
			op.assertJvmOperationTypeParameterBindings(expectedTypeParameterDeclarator)
		}
	}
	
	/**
	 * Retrieves all the JvmOperations associated to the last entity
	 */
	private def associatedJvmOperations(Program it) {
		val programElement = entities.last
		programElement.jvmElements.filter(JvmGenericType).
				map[declaredOperations].flatten
	}

	
	def private assertJvmOperationTypeParameterBindings(JvmOperation op, (JvmTypeParameterDeclarator)=>JvmTypeParameterDeclarator expectedTypeParameterDeclarator) {
		if (!op.simpleName.startsWith("set")) {
			// the setter method is void
			op.assertJvmTypeParameterBinding(op.returnType, expectedTypeParameterDeclarator)
		}
		for (p : op.parameters) {
			op.assertJvmTypeParameterBinding(p.parameterType, expectedTypeParameterDeclarator)
		}
		assertJvmTypeParameterDeclaratorTypeParameterBindings(op, expectedTypeParameterDeclarator)
	}

	def private assertJvmGenericTypeTypeParameterBindings(JvmGenericType t, (JvmTypeParameterDeclarator)=>JvmTypeParameterDeclarator expectedTypeParameterDeclarator) {
		assertJvmTypeParameterDeclaratorTypeParameterBindings(t, expectedTypeParameterDeclarator)
	}
	
	private def assertJvmTypeParameterDeclaratorTypeParameterBindings(JvmTypeParameterDeclarator t, (JvmTypeParameterDeclarator)=>JvmTypeParameterDeclarator expectedTypeParameterDeclarator) {
		for (typePar : t.typeParameters) {
			assertJvmConstraintsTypeParameterBindings(t, typePar, expectedTypeParameterDeclarator)
		}
	}
	
	private def assertJvmConstraintsTypeParameterBindings(JvmTypeParameterDeclarator t, JvmConstraintOwner constraintOwner, (JvmTypeParameterDeclarator)=>JvmTypeParameterDeclarator expectedTypeParameterDeclarator) {
		for (c : constraintOwner.constraints) {
			t.assertJvmTypeParameterBinding(c.typeReference, expectedTypeParameterDeclarator)
		}
	}

	def private assertJvmTypeParameterBinding(JvmTypeParameterDeclarator op, JvmTypeReference typeRef, (JvmTypeParameterDeclarator)=>JvmTypeParameterDeclarator expectedTypeParameterDeclarator) {
		// can be either a JvmGenericType or a JvmOperation in case of
		// a method with a generic type
		val typeParDeclarator = expectedTypeParameterDeclarator.apply(op)
		
		if (typeRef instanceof JvmParameterizedTypeReference) {
			assertJvmTypeParameterBindingAgainstTypeParDeclarator(
				op, typeRef, typeParDeclarator, "JvmParameterizedTypeReference"
			)
		} else if (typeRef instanceof XFunctionTypeRef) {
			assertJvmTypeParameterBindingAgainstTypeParDeclarator(
				op, typeRef.returnType, typeParDeclarator,
				"XFunctionTypeRef.returnType"
			)
			for (p : typeRef.paramTypes) {
				assertJvmTypeParameterBindingAgainstTypeParDeclarator(
					op, p, typeParDeclarator,
					"XFunctionTypeRef.paramType"
				)
			}
		} else {
			fail("Unknown JvmTypeReference: " + typeRef)
		}
	}
	
	private def void assertJvmTypeParameterBindingAgainstTypeParDeclarator(
		JvmTypeParameterDeclarator op, JvmTypeReference jvmTypeRef,
		JvmTypeParameterDeclarator typeParDeclarator, String desc
	) {
		val typeParams = typeParDeclarator.typeParameters
		
		for (typeParam : typeParams) {
			val type = jvmTypeRef.type
		
			if (type instanceof JvmTypeParameter) {
				assertTypeParameterBinding(
					op, jvmTypeRef,
					typeParams.typeParamByName(jvmTypeRef), type, desc
				)
			} else if (type instanceof JvmGenericType) {
				val typeArgs = (jvmTypeRef as JvmParameterizedTypeReference).arguments
				for (typeArg : typeArgs) {
					if (typeArg instanceof JvmWildcardTypeReference) {
						for (c : typeArg.constraints) {
							op.assertJvmTypeParameterBindingAgainstTypeParDeclarator(
								c.typeReference, typeParDeclarator, "Wildcard." + desc
							)
						}
					} else {
						if (typeArg != null) {
							assertTypeParameterBinding(
								op, jvmTypeRef,
								typeParams.typeParamByName(typeArg), 
								typeArg.type, desc
							)
						}
					}
				}
			} else {
				fail("Unknown JvmType: " + type)
			}	
		}
	}

	private def typeParamByName(List<JvmTypeParameter> typeParams, JvmTypeReference typeRef) {
		val typePar = typeParams.findFirst[ tp | tp.simpleName == typeRef.type.simpleName ]
		assertNotNull("could not find " + typeRef.type.simpleName, typePar)
		return typePar
	}
	
	private def assertTypeParameterBinding(JvmTypeParameterDeclarator e, JvmTypeReference jvmTypeRef, JvmTypeParameter expectedTypePar, JvmType actualType, String desc) {
		val id = switch (e) {
			JvmOperation: "\nop: " + e.identifier
			JvmGenericType: "\njava type: " + e.identifier
		}
		
		assertSame(
			desc +
			id + ",\n"
			+ jvmTypeRef.type + "\nbound to\n" +
			expectedTypePar + "\n",
			expectedTypePar,
			actualType
		)
	}

}