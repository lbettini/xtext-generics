package org.xtext.example.generics.validation;

import static org.eclipse.xtext.xbase.validation.IssueCodes.STATIC_ACCESS_TO_INSTANCE_MEMBER;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeParameter;
import org.eclipse.xtext.common.types.JvmTypeParameterDeclarator;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.TypesPackage;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.XClosure;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations;

import com.google.inject.Inject;


/**
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=468174
 * 
 * @author Lorenzo Bettini
 *
 */
public class PatchedXbaseValidator extends AbstractGenericsValidator {
	
	@Inject
	private IJvmModelAssociations associations;

	@Override
	@Check 
	public void checkTypeParameterNotUsedInStaticContext(JvmTypeReference ref) {
		JvmType type = ref.getType();
		if(type instanceof JvmTypeParameter) {
			JvmTypeParameter typeParameter = (JvmTypeParameter) type;
			JvmTypeParameterDeclarator declarator = typeParameter.getDeclarator();
			if (!(declarator instanceof JvmOperation) || !isTypeParameterOfClosureImpl(ref)) {
				if (!findAssociatedContainingJvmTypeParameterDeclarator(ref)) {
					error("Cannot make a static reference to the non-static type " + typeParameter.getName(), 
							ref, TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE, -1, STATIC_ACCESS_TO_INSTANCE_MEMBER);
				}
			}
		}
	}

	private boolean findAssociatedContainingJvmTypeParameterDeclarator(
			JvmTypeReference ref) {
		EObject container = ref.eContainer();
		final JvmTypeParameterDeclarator typeDeclarator = ((JvmTypeParameter) ref
				.getType()).getDeclarator();

		// we can't assume that the referred type parameter is declared in
		// logicalContainerProvider.getNearestLogicalContainer(ref)
		// since there can be several inferred Jvm elements for the same
		// DSL model originally declaring type parameters.
		// So we must check all the inferred Jvm elements in the containment hierarchy
		// till we find the corresponding JvmTypeParameterDeclarator
		// https://bugs.eclipse.org/bugs/show_bug.cgi?id=468174

		while (container != null) {
			if (associations.getJvmElements(container).contains(typeDeclarator)) {
				return true;
			}

			container = container.eContainer();
		}

		return false;
	}

	private boolean isTypeParameterOfClosureImpl(JvmTypeReference ref) {
		JvmFormalParameter parameter = EcoreUtil2.getContainerOfType(ref, JvmFormalParameter.class);
		if (parameter != null) {
			return parameter.eContainer() instanceof XClosure;
		}
		return false;
	}

	
}
