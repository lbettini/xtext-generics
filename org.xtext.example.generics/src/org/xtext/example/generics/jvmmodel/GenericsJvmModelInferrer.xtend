package org.xtext.example.generics.jvmmodel

import com.google.inject.Inject
import java.util.List
import org.eclipse.xtend2.lib.StringConcatenationClient
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmTypeParameter
import org.eclipse.xtext.common.types.JvmTypeParameterDeclarator
import org.eclipse.xtext.common.types.JvmUpperBound
import org.eclipse.xtext.common.types.TypesFactory
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociator
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder
import org.xtext.example.generics.generics.GenericEntity
import org.xtext.example.generics.generics.Operation
import org.xtext.example.generics.generics.Property

/**
 * <p>Infers a JVM model from the source model.</p> 
 *
 * <p>The JVM model should contain all elements that would appear in the Java code 
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>     
 */
class GenericsJvmModelInferrer extends AbstractModelInferrer {

    /**
     * convenience API to build and initialize JVM types and their members.
     */
	@Inject extension JvmTypesBuilder
	@Inject extension GenericsJvmModelUtil
	@Inject	IJvmModelAssociator associator
	@Inject TypesFactory typesFactory

   	def dispatch void infer(GenericEntity entity, extension IJvmDeclaredTypeAcceptor acceptor, boolean isPreIndexingPhase) {
   		accept(entity.toInterface( entity.entityInterfaceFQN ) []) [
			documentation = entity.documentation

			if (entity.superType != null)
				superTypes += entity.superType.cloneWithProxies()

			copyTypeParameters(entity.typeParameters)
			
			// now let's go over the features
			for ( f : entity.features ) {
				switch f {
			
					// for properties we create a getter and a setter
					Property : {
						members += f.toGetter(f.name, f.type) => [
							// to make it abstract
							body = null as StringConcatenationClient
						]
						members += f.toSetter(f.name, f.type) => [
							body = null as StringConcatenationClient
						]
					}
			
					// operations are mapped to methods
					Operation : {
						members += f.toMethod(f.name, f.type ?: inferredType) [
							documentation = f.documentation

							copyTypeParameters(f.typeParameters)

							abstract = true
							for (p : f.params) {
								parameters += p.cloneWithProxies
							}
						]
					}
				}
			}
		]

   		accept(entity.toClass( entity.entityClassFQN )) [
			documentation = entity.documentation

			copyTypeParameters(entity.typeParameters)
			
			superTypes += entityInterfaceTypeRef(entity)

			// now let's go over the features
			for ( f : entity.features ) {
				switch f {
			
					// for properties we create a field, a getter and a setter
					Property : {
						val field = f.toField(f.name, f.type)
						members += field
						members += f.toGetter(f.name, f.type)
						members += f.toSetter(f.name, f.type)
					}
			
					// operations are mapped to methods
					Operation : {
						members += f.toMethod(f.name, f.type ?: inferredType) [
							documentation = f.documentation

							copyTypeParameters(f.typeParameters)

							for (p : f.params) {
								parameters += p.cloneWithProxies
							}
							// here the body is implemented using a user expression.
							// Note that by doing this we set the expression into the context of this method, 
							// The parameters, 'this' and all the members of this method will be visible for the expression. 
							body = f.body
						]
					}
				}
			}
		]
   	}

	def private entityInterfaceTypeRef(JvmGenericType inferredType, GenericEntity e) {
		e.entityInterfaceFQN.typeRef(
			// type parameters of the inferred Java class are transformed
			// into type arguments for the type reference to the inferred Java interface
			inferredType.typeParameters.map[typeRef]
		)
	}

	def private void copyTypeParameters(JvmTypeParameterDeclarator target, List<JvmTypeParameter> typeParameters) {
		for (typeParameter : typeParameters) {
			val clonedTypeParameter = typeParameter.cloneWithProxies();
			if (clonedTypeParameter != null) {
				target.typeParameters += clonedTypeParameter
				associator.associate(typeParameter, clonedTypeParameter);
			}
		}
		target.fixTypeParameters
	}

	def private void fixTypeParameters(JvmTypeParameterDeclarator target) {
		for (typeParameter : target.getTypeParameters()) {
			var upperBoundSeen = 
				typeParameter.constraints.exists[it instanceof JvmUpperBound]
			if (!upperBoundSeen) {
				val upperBound = typesFactory.createJvmUpperBound();
				upperBound.setTypeReference(typeRef(Object))
				typeParameter.getConstraints().add(upperBound);
			}
		}
	}
	
}

