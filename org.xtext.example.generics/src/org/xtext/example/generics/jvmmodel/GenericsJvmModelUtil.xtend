package org.xtext.example.generics.jvmmodel

import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.xtext.example.generics.generics.GenericEntity

class GenericsJvmModelUtil {
	
	@Inject extension IQualifiedNameProvider

	def entityInterfaceFQN(GenericEntity e) {
		e.fullyQualifiedName.toString
	}

	def entityClassFQN(GenericEntity e) {
		e.fullyQualifiedName + "Impl"
	}
}